setup:
	@[ -d "bin" ] || ( echo ">> use \"make install\" to install KAFKA"; exit 1 )
	@$(MAKE) start
	@echo ">> use \"make start\" to start servers"
	@echo ">> use \"make stop\" to stop servers"
	@echo ">> use \"sudo make startup\" to install startup script"
	@echo ">> use \"make topics\" to list all existing topics"
	@echo ">> use \"make groups\" to list all existing groups"
	@echo ">> use \"make create topic=XXX\" to create a topic"
	@echo ">> use \"make delete topic=XXX\" to delete an existing topic"

bin: install

distclean: clean
	@rm -rf NOTICE LICENSE config bin libs licenses site-docs logs
clean:
	@rm -rf kafka.tgz

port = 9092
hostname = localhost
source = https://downloads.apache.org/kafka/3.6.1/kafka-3.6.1-src.tgz
download:
	@[ "${source}" ] || ( echo ">> \"source\" variable is not set"; exit 1 )
	@echo "Downloading.. $(source)"
	@$(MAKE) source=$(source) kafka.tgz

kafka.tgz:
	@[ "${source}" ] || ( echo ">> \"source\" variable is not set"; exit 1 )
	@wget $(source) -O kafka.tgz

startup:
	@[ "${KAFKA}" ] || ( echo ">> \"KAFKA\" variable must be specified"; exit 1 )
	@cp etc/init.d/kafka /etc/init.d
	@sed -i 's,<location>,${KAFKA},g' /etc/init.d/kafka
	@sed -i 's,<user>,${USER},g' /etc/init.d/kafka
	@chmod 755 /etc/init.d/kafka

install: kafka.tgz
	@tar -xzf kafka.tgz --strip-components=1
	@rm -rf kafka.tgz
	@./gradlew jar -PscalaVersion=2.13.11
	@test ! -f zookeeper.properties && cp config/zookeeper.properties . || echo -n "\n>> zookeeper configuration already exists"
	@rm config/zookeeper.properties
	@ln -s ../zookeeper.properties ./config/
	@test ! -f    server.properties && cp config/server.properties . || echo -n "\n>> kafka configuration already exists"
	@rm config/server.properties
	@ln -s ../server.properties ./config/
	@echo ""

restart: stop start
start:
	@echo "Starting ZooKeeper & KAFKA"
	@bin/zookeeper-server-start.sh -daemon config/zookeeper.properties
	@bin/kafka-server-start.sh -daemon config/server.properties
stop:
	@echo "Stopping ZooKeeper & KAFKA"
	@bin/zookeeper-server-stop.sh
	@bin/kafka-server-stop.sh

topics:
	@$(KAFKA)/bin/kafka-topics.sh --list  --bootstrap-server $(hostname):$(port)
groups:
	@$(KAFKA)/bin/kafka-consumer-groups.sh --list --bootstrap-server $(hostname):$(port)

create:
	@[ "${topic}" ] || ( echo ">> \"topic\" variable is not set"; exit 1 )
	@echo "Creating KAFKA topic \"$(topic)\""
	@bin/kafka-topics.sh --create --topic $(topic) --bootstrap-server $(hostname):$(port)
delete:
	@[ "${topic}" ] || ( echo ">> \"topic\" variable is not set"; exit 1 )
	@echo "Deleting KAFKA topic \"$(topic)\""
	@bin/kafka-topics.sh --delete --topic $(topic) --bootstrap-server $(hostname):$(port)
